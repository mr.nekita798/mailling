from rest_framework import serializers
from main.models import Mailing, Client, Message
from django.core.exceptions import ValidationError

class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = "__all__"

    def validate(self, data):
        instance = self.Meta.model(**data)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError({"error":e.message_dict})
        return data





class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

