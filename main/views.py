from django.shortcuts import render
from main.models import Client,Message,Mailing
from main.serializers import ClientSerializer,MessageSerializer,MailingSerializer
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from rest_framework.generics import GenericAPIView
class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

class MessageVewSet(GenericAPIView):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()

class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        all_messages = Message.objects.filter(mailing_id = instance.id)
        hold = []
        sent = []
        for message in all_messages:
            data = MessageSerializer(message).data
            hold.append(data) if message.status == 1 else sent.append(data)
        serializer_data = dict(serializer.data)
        serializer_data['messages']= {
            "Отправленные":{
                    'Количество': len(hold),
                    'Сообщения': hold
                    },
            "В ожидание":{
                    'Количество': len(sent),
                    'Сообщения': sent
                    },
            }
        return Response(serializer_data)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)


        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data

        mailing_ids = set([mail['id'] for mail in serializer_data])

        messages = Message.objects.filter(mailing_id__in=mailing_ids).values('mailing_id', 'status')

        sent_dict = {}
        waiting_dict = {}
        for message in messages:
            if message['status']:
                if message['mailing_id'] in sent_dict:
                    sent_dict[message['mailing_id']] += 1
                else:
                    sent_dict[message['mailing_id']] = 1
            else:
                if message['mailing_id'] in waiting_dict:
                    waiting_dict[message['mailing_id']] += 1
                else:
                    waiting_dict[message['mailing_id']] = 1

        for mail in serializer_data:
            mailing_id = mail['id']
            mail['Отправлено'] = sent_dict.get(mailing_id, 0)
            mail['В ожидание'] = waiting_dict.get(mailing_id, 0)


        return Response(serializer_data)


