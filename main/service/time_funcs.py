import pytz
from datetime import datetime, timedelta
from collections import defaultdict


time_zone=pytz.timezone('UTC')


def convert_seconds(seconds):
    days = seconds // 86400
    if days < 0:
        days = 0
    hours = (seconds % 86400) // 3600
    minutes = ((seconds % 86400) % 3600) // 60
    seconds = ((seconds % 86400) % 3600) % 60

    if hours == 0:
        return f"{minutes}:{seconds:02d}"
    elif days == 0:
        return f"{hours}:{minutes:02d}:{seconds:02d}"
    else:
        return f"{days} дней, {hours}:{minutes:02d}:{seconds:02d}"
def get_zones():
    utc_now = datetime.utcnow()
    timezones = pytz.common_timezones
    zones = defaultdict(list)
    for zone in timezones:
        offset = str(int(pytz.timezone(zone).utcoffset(utc_now) / timedelta(hours=1)))
        zone_info = f"{zone.split('/GMT')[0]}: GMT {offset if offset == '0' else '+ ' + offset if '-' not in offset else offset}"
        zones[int(offset)].append((zone, zone_info))

    sorted_zones = sorted(zones.items(), key=lambda x: x[0], reverse=False)
    result = []
    for k, v in sorted_zones:
        result.extend(sorted(v))

    return result


def time_handle(message_data):
    tz = pytz.timezone(message_data.get('timezone'))
    time_now = datetime.now(tz)
    date_start = message_data.get('date_start')
    date_end = message_data.get('date_end')
    time_allow_start = message_data.get('allow_send_start')
    time_allow_stop = message_data.get('allow_send_stop')

    possible_to_send = True

    if time_now >= date_start > date_end:
        time_left = 0
        time_client = time_now.time()
        if time_allow_start <= time_client < time_allow_stop:
            pass
        elif time_client < time_allow_start:
            time_left = (datetime.combine(time_now.date(),time_allow_start) - datetime.combine(time_now.date(),time_client)).total_seconds()
        else:
            time_left = (datetime.combine(time_now.date(), time_allow_start) + timedelta(days=1) - datetime.combine(datetime.now().date(),time_client)).total_seconds()
    else:
        time_left = (date_start - time_now).total_seconds()
        time_client = (time_now + timedelta(seconds=time_left)).time()
        if time_allow_start <= time_client < time_allow_stop:
            pass
        elif time_client < time_allow_start:
            time_left += (datetime.combine(time_now.date(), time_allow_start) - datetime.combine(time_now.date(), time_client)).total_seconds()
        else:
            time_left += (datetime.combine(time_now.date(), time_allow_start) - datetime.combine(time_now.date(), time_client)).total_seconds()

    if datetime.now(time_zone) + timedelta(seconds=time_left) > date_end:
        possible_to_send = False
    if time_left<0:
        time_left=0
    return possible_to_send , int(time_left)
