from django.dispatch import receiver
from main.models import Mailing,Client,Message,Filter_user
from django.db.models.signals import post_save
from django.db.models import Q
from main.tasks.tasks import send_message_task
from main.service.time_funcs import time_handle, convert_seconds
from loguru import logger

def get_to_dict(client,mail):
    fields = {
        'client':['id','phone_number','timezone'],
        'mail':['message_text','date_start','date_end','allow_send_start','allow_send_stop']
    }
    data_for_message = {}
    for model in fields:
        for field in fields[model]:
            data_for_message[field] = eval(f'{model}.{field}')
    return data_for_message

@receiver(signal=post_save,sender = Client)
def save_filter(**kwargs):
    client = kwargs.get('instance')
    try:
        Filter_user.objects.create(mobile_code = client.mobile_code, teg = client.teg)
    except Exception as e:
        print('Данная пара уже создана')

@receiver(signal=post_save, sender=Mailing)
def send_message(**kwargs):
    if kwargs.get('created'):
        mail = kwargs.get('instance')
        filter_clients = kwargs.get('instance').filter
        if not filter_clients.teg:
            clients = Client.objects.filter(mobile_code=filter_clients.mobile_code)
        else:
            clients = Client.objects.filter(Q(mobile_code = filter_clients.mobile_code) & Q(teg=filter_clients.teg))
        for client in clients:
            message_data = get_to_dict(client,mail)
            possible, execute_in_time = time_handle(message_data)
            if possible:
                message_created= Message.objects.create(client_id = client.id, mailing_id = mail.id)
                message_data['message_id'] = message_created.id
                send_message_task.apply_async(
                    kwargs={
                        'message_data': {
                            'phone': message_data['phone_number'],
                            'id': message_created.id,
                            'text': mail.message_text,
                        },
                        'mail_id': mail.id
                    },
                    countdown=execute_in_time
                )

                msg_logger = f'Отправка сообщения через: {convert_seconds(execute_in_time)}' if execute_in_time else 'Происходит отправка сообщения'
                logger.info(f'Клиент: {client.id} Сообщение: {message_data["message_id"]}. {msg_logger}')
            else:
                logger.error(f'Отправка Клиенту {client.id}. Рассылка: {mail.id} - невозможна.')



