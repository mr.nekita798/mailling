from loguru import logger
import time
import json
class RequestLoggerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        level = 'error' if response.status_code > 400 else 'info'
        try:
            user = f"| User: {request.user.username}"
        except:
            user = ''
        if response.reason_phrase == 'Created':
            logger.info(f'{response.status_code}  {request.method} {response.reason_phrase} {request.path} id / {response.data["id"]}. {user}')
        else:
            eval(f'logger.{level}("{response.status_code}  {request.method} {response.reason_phrase} {request.path} {user}")')
        return response