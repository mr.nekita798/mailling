from django.contrib import admin

from django.contrib import admin
from main.models import Mailing, Client


admin.site.register(Client)
admin.site.register(Mailing)
