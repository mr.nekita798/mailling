import json
from config.celery import app
from loguru import logger
from config.settings import TOKEN, URL
from main.models import Message
import requests

headers = {
        'Authorization': f'Bearer {TOKEN}',
        'Content-Type': 'application/json',
        'accept':'application/json',

}

@app.task(
        bind = True,
        retry_backoff = 5,
        retry_jitter = True,
        retry_kwargs = {'max-retries':7},
        serializer = 'json')
def send_message_task(self,mail_id,**kwargs):
    message_data = kwargs.get('message_data')
    logger.info(message_data)
    try:
        response = requests.post(URL+f'{message_data["id"]}', headers=headers, json=message_data)
    except Exception as e:
        logger.critical(f"Сообщение: {message_data['id']}. Проблема с отправлением запроса: {e}")
        raise self.retry(countdown=30)

    if response.status_code == 200:
            Message.objects.filter(pk=message_data['id']).update(status=1)
            logger.info(f'Сообщение: {message_data["id"]} отправлено')
    else:
        if response.status_code == 401:
            logger.critical(f"Сообщение: {message_data['id']}. Неверный токен")
            raise self.retry(expires=360)
        elif response.status_code == 400:
            logger.error(
                f"Сообщение {message_data['id']}. Неверный запрос. Запрос: {json.dumps(message_data)}")
            raise self.retry(countdown=20)
        else:
            logger.error(f"Сообщение: {message_data['id']} недоставлено. Код ответа: {response.status_code}")
            raise self.retry(countdown = 30)




