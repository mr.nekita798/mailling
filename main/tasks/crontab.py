from datetime import datetime
from config.celery import app
from loguru import logger
from django.template.loader import get_template
from django.template import Context
from config.settings import EMAIL_GET_STATS,EMAIL_HOST_USER
from main.models import Message,Mailing

from django.core.mail import EmailMessage

@app.task
def send_beat_email():
    today = datetime.now().date()
    messages = [i for i in Message.objects.filter(status=1) if i.date_created.date() == today]
    mailings = [{
        'id': mail.id,
        'sent': len([True for message in messages if message.mailing_id == mail.id])
    } for mail in Mailing.objects.all() if mail.date_start.date() == today]
    body = get_template("email.html").render({
    'mailings': mailings,
    'today': today,
    })
    mail = EmailMessage(
            subject= f'Статистика за {today}',
            body=body,
            from_email = EMAIL_HOST_USER,
            to = [EMAIL_GET_STATS],
            reply_to = [EMAIL_HOST_USER]
    )
    mail.content_subtype = "html"
    mail.send()
    logger.info(f"Статистика за {today} успешно отправлена")