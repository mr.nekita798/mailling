from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator, ValidationError
from django.utils import timezone
from main.service.time_funcs import get_zones
# Create your models here.

class Filter_user(models.Model):
    mobile_code = models.CharField(verbose_name=_('Код мобильного оператора'),
                                   validators=[RegexValidator(r'^\d+$', 'Мобильный код должен состоять только из цифр')], max_length=3)
    teg = models.CharField(verbose_name=_("Тег для поиска"), max_length=255, blank=True)

    class Meta:
        verbose_name = _('фильтр')
        verbose_name_plural = _('фильтра')
        unique_together = ('mobile_code', 'teg')


    def __str__(self):
        operator = f"Оператор: {self.mobile_code}" if self.mobile_code else ""
        tag = f"Тег: {self.teg}" if self.teg else ""
        return f"{operator}. {tag}".strip()



class Mailing(models.Model):
    date_start = models.DateTimeField(verbose_name=_('Дата отправления'))
    message_text = models.TextField(verbose_name=_('Текст сообщения'))
    filter = models.ForeignKey(Filter_user, related_name='filter_user', verbose_name=_('Фильтр свойств клиента'), on_delete=models.DO_NOTHING)
    date_end = models.DateTimeField(_('Дата окончания'))
    allow_send_start = models.TimeField(_('Разрешено отправление'), blank=True, null=True)
    allow_send_stop = models.TimeField(_('Не разрешенно отправление'), blank=True, null=True)
    error_messages = {
                "date_start": {
                    "past_time": {
                        "msg":"Время не может быть указано в прошлом.",
                        "title": "Дата отправления",
                        "field": "date_start"
                    }
                },
                "date_end": {
                    "past_time": {
                        "msg":"Время не может быть указано в прошлом.",
                        "title": "Дата окончания",
                        "field": "date_end"
                    },
                    "start_end": {
                        "msg":"Дата конца рассылки не может быть раньше ее начала.",
                        "title": "Дата окончания",
                        "field": "date_end"
                    }
                },
                "allow_send_start": {
                    "no_stop": {
                        "msg": "Не указано время окончания разрешенного отправления.",
                        "title":"Не разрешенно отправление",
                        "field": "allow_send_stop"
                        },
                    "start_stop": {
                        "msg":"Время начала отправления должно быть раньше времени окончания.",
                        "title": "Разрешено отправление",
                        "field": "allow_send_start"
                    }
                },
                "allow_send_stop": {
                    "no_start": {
                        "msg":"Не указано время начала разрешенного отправления.",
                        "title":"Разрешено отправление",
                        "field": "allow_send_start"
                    }
                }
            }


    class Meta:
        verbose_name = _('рассылка')
        verbose_name_plural= _('рассылки')


    def clean(self):
        now = timezone.now()
        if self.date_start < now:
            raise ValidationError(self.error_messages['date_start']['past_time'])

        if self.date_end < now:
            raise ValidationError(self.error_messages['date_end']['past_time'])

        if self.date_start > self.date_end:
            raise ValidationError(self.error_messages['date_end']['start_end'])

        if self.allow_send_start and not self.allow_send_stop:
            raise ValidationError(self.error_messages['allow_send_start']['no_stop'])

        elif not self.allow_send_start and self.allow_send_stop:
            raise ValidationError(self.error_messages['allow_send_stop']['no_start'])

        # elif self.allow_send_start and self.allow_send_stop:
        #     if self.allow_send_start >= self.allow_send_stop:
        #         raise ValidationError(self.error_messages['allow_send_start']['start_stop'])

        super().clean()

    def __str__(self):
        format = '%d.%m.%Y %H:%M'
        return f"Начало: {self.date_start.strftime(format)}. Конец: {self.date_end.strftime(format)}"



class Client(models.Model):
    zones = get_zones()
    phone_number = models.CharField(verbose_name=_('Номер телефона'), max_length=11, unique=True, validators=[RegexValidator(r'^7\d{10}$','Номер телефона должен быть в формате: 7XXXXXXXXXX')])
    mobile_code = models.CharField(verbose_name=_('Код мобильного оператора'), max_length=3)
    teg = models.CharField(verbose_name=_("Тег"), max_length=255, blank=True)
    timezone = models.CharField(verbose_name=_('Часовой пояс'), choices=zones, default='Europe/Samara', blank=True, max_length=255)

    class Meta:
        verbose_name = _('клиент')
        verbose_name_plural = _('клиенты')

    def __str__(self):
        return f"Клиент: {self.phone_number}"

class Message(models.Model):

    class Status(models.IntegerChoices):
        FAILED = -1, _('Неуспешно')
        HOLD = 0, _('В ожидание отправки')
        OUT = 1, _('Отправлено')

    date_created = models.DateTimeField(verbose_name=_('Дата отправления'), auto_now_add=True)
    status = models.IntegerField(choices=Status.choices, default=0)
    client = models.ForeignKey(Client, related_name='client', verbose_name='Клиент', on_delete=models.CASCADE)
    mailing = models.ForeignKey(Mailing, related_name='mailing', verbose_name='Рассылка', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('сообщение')
        verbose_name_plural = _('сообщения')

    def __str__(self):
        return f'{self.status}: {self.client} {self.mailing}'








