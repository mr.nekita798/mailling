from datetime import datetime
import datetime
import pytz
from django.test import TestCase
from main.service.time_funcs import time_handle


class TestTimeHandle(TestCase):
    def setUp(self):
        self.message_data = {
            'timezone': 'Europe/Moscow',
            'date_start': datetime.datetime.now(pytz.timezone('Europe/Moscow')) + datetime.timedelta(days=1),
            'date_end': datetime.datetime.now(pytz.timezone('Europe/Moscow')) + datetime.timedelta(days=3),
            'allow_send_start': datetime.time(8, 0),
            'allow_send_stop': datetime.time(20, 0)
        }

    def test_possible_to_send(self):
        self.assertTrue(time_handle(self.message_data)[0])

    def test_time_left(self):
        time_left = time_handle(self.message_data)[1]
        self.assertGreater(time_left, 0)
        self.assertIsInstance(time_left, int)

    def test_possible_to_send_allowed_time(self):
        self.message_data['allow_send_start'] = datetime.time(0, 0)
        self.message_data['allow_send_stop'] = datetime.time(23, 59)
        self.assertTrue(time_handle(self.message_data)[0])

    def test_not_possible_to_send(self):
        self.message_data['date_start'] = datetime.datetime.now(pytz.timezone('Europe/Moscow')) + datetime.timedelta(days=1)
        self.message_data['date_end'] = datetime.datetime.now(pytz.timezone('Europe/Moscow')) - datetime.timedelta(days=1)
        self.assertFalse(time_handle(self.message_data)[0])
