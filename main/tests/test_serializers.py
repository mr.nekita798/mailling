from django.test import TestCase
from main.models import Mailing,Filter_user
from main.serializers import ClientSerializer,MailingSerializer
from datetime import datetime
from pytz import timezone




class ClientSerializerTest(TestCase):
    def setUp(self):
        self.client_data = {
            "phone_number": "79123456789",
            "mobile_code": "111",
            "teg": "тест",
            "timezone": "Europe/Moscow"
        }

    def test_serializer_with_valid_data(self):
        serializer = ClientSerializer(data=self.client_data)
        self.assertTrue(serializer.is_valid())
        result = serializer.validated_data
        self.assertEqual(result["phone_number"], "79123456789")
        self.assertEqual(result["mobile_code"], "111")

    def test_serializer_with_invalid_data(self):
        client_data = {
            "phone_number": "invalid_phone_number",
            "mobile_code": "111",
            "teg": "тест",
            "timezone": "Europe/Moscow"
        }
        serializer = ClientSerializer(data=client_data)
        self.assertFalse(serializer.is_valid())

class MailingSerializerTest(TestCase):
    def setUp(self):
        self.filter = Filter_user.objects.create(mobile_code = '152')
        moscow_tz = timezone('Europe/Moscow')

        self.valid_data = {
            "date_start": moscow_tz.localize(datetime.strptime("2026-01-01 23:59:59", "%Y-%m-%d %H:%M:%S")).astimezone(
                timezone('Europe/Moscow')),
            "message_text": "Тестовое сообщение", "filter": self.filter,
            "date_end": moscow_tz.localize(datetime.strptime("2028-01-01 23:59:59", "%Y-%m-%d %H:%M:%S")).astimezone(
                timezone('Europe/Moscow'))}

        self.invalid_data = {
            "date_start": moscow_tz.localize(datetime.strptime("2025-01-01 00:00:00", "%Y-%m-%d %H:%M:%S")),
            "message_text": "Тестовое сообщение", "filter": self.filter,
            "date_end": moscow_tz.localize(datetime.strptime("2023-01-01 00:00:00", "%Y-%m-%d %H:%M:%S")).astimezone(
                timezone('Europe/Moscow'))}
        self.serializer = MailingSerializer()

    def test_valid_data(self):
        mailing = Mailing(**self.valid_data)
        mailing.full_clean()


    def test_invalid_data(self):
        mailing = Mailing(**self.invalid_data)