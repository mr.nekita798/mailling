from django.test import TestCase
from django.db.utils import IntegrityError
from main.models import Filter_user, Mailing, Client, Message
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
import pytz
from datetime import timedelta,datetime

class FilterUserTest(TestCase):

    def setUp(self):
        self.filter_user = Filter_user.objects.create(mobile_code='902', teg='tag3')


    def test_unique_together(self):
        with self.assertRaises(IntegrityError):
            Filter_user.objects.create(mobile_code='902', teg='tag3')

    def test_str_method(self):
        expected_str = 'Оператор: 902. Тег: tag3'
        self.assertEqual(expected_str, str(self.filter_user))


class MailingTest(TestCase):
    def setUp(self):
        self.filter_user = Filter_user.objects.create(mobile_code='902', teg='tag3')
        self.now = timezone.now()

    def test_clean_date_start_in_past(self):
        yesterday = self.now - timezone.timedelta(days=1)
        mailing = Mailing(date_start=yesterday, message_text="Тест", filter=self.filter_user, date_end=self.now)

        with self.assertRaises(ValidationError):
            mailing.clean()

    def test_clean_date_end_in_past(self):
        yesterday = self.now - timezone.timedelta(days=1)
        mailing = Mailing(date_start=self.now, message_text="Тест", filter=self.filter_user, date_end=yesterday)

        with self.assertRaises(ValidationError):
            mailing.clean()

    def test_clean_date_end_before_date_start(self):
        tomorrow = self.now + timezone.timedelta(days=1)
        mailing = Mailing(date_start=tomorrow, message_text="Тест", filter=self.filter_user, date_end=self.now)

        with self.assertRaises(ValidationError):
            mailing.clean()

    def test_clean_no_allow_send_start_with_allow_send_stop(self):
        mailing = Mailing(date_start=self.now, message_text="Тест", filter=self.filter_user, date_end=self.now, allow_send_stop=self.now.time())

        with self.assertRaises(ValidationError):
            mailing.clean()

    def test_clean_no_allow_send_stop_with_allow_send_start(self):
        mailing = Mailing(date_start=self.now, message_text="Тест", filter=self.filter_user, date_end=self.now, allow_send_start=self.now.time())

        with self.assertRaises(ValidationError):
            mailing.clean()


    def test_str_method(self):
        expected_str = f"Начало: {self.now.strftime('%d.%m.%Y %H:%M')}. Конец: {self.now.strftime('%d.%m.%Y %H:%M')}"
        mailing = Mailing(date_start=self.now, message_text="Тест", filter=self.filter_user, date_end=self.now)
        self.assertEqual(str(mailing), expected_str)


class ClientTestCase(TestCase):
    def test_invalid_phone_number(self):
        validator = RegexValidator(r'^7\d{10}$', 'Номер телефона должен быть в формате: 7XXXXXXXXXX')
        with self.assertRaises(ValidationError):
            validator('89999999999')



    def test_str_method(self):
        expected_str = 'Клиент: 79991234567'
        client = Client(phone_number='79991234567', mobile_code='999')
        self.assertEqual(str(client), expected_str)


class MessageTest(TestCase):

    def setUp(self):
        self.filter = Filter_user.objects.create(mobile_code='902', teg='tag3')
        self.client = Client.objects.create(phone_number='79991234567', mobile_code='12345')
        self.mailing = Mailing.objects.create(date_start=datetime.datetime.now(pytz.timezone('Europe/Moscow')) + timedelta(days=1), date_end=datetime.now(pytz.timezone('Europe/Moscow')) + timedelta(days=3),
                                              message_text='Тест', filter=Filter_user.objects.filter(mobile_code='902').first())

